<!-- https://www.tablesgenerator.com/markdown_tables# -->
# Documentation Battleships Game
This documentation shows all functionalities of the battleships game.

In the following the section the message structure is presented with its possible types.
The message is used to communicate between client and server. It is implemented within the Message class.


## Message Structure
|         | bytes |
|---------|-------|
| type    | 1     |
| length  | 1     |
| payload | x     |
| crc8    | 1     |

type: Determines message type
length: Determines length of payload in bytes
payload: Data that is sent with the message
crc8: Checksum to check if the message is correct

## Message types
|                     | type | length | payload                       |
|---------------------|------|--------|-------------------------------|
| SESSION_REFUSED     | 0    | 0      |                               |
| SESSION_WAITING     | 1    | 0      |                               |
| SESSION_STARTED     | 2    | 0      |                               |
| SESSION_CLOSED      | 3    | 0      |                               |
| SESSION_KEEP_ALIVE  | 4    | 0      |                               |
| SESSION_STILL_ALIVE | 5    | 0      |                               |
| SESSION_STATUS      | 6    | 0      |                               |
| MESSAGE_OK          | 10   | 0      |                               |
| MESSAGE_BROKEN      | 11   | 0      |                               |
| GAME_INIT           | 20   | 1      | client_id                     |
| GAME_START          | 21   | 100    | field[]                       |
| GAME_TURN           | 22   | 1      | player_id                     |
| GAME_SHOOT          | 23   | 3      | shooter_id, pos_x, pos_y      |
| GAME_HIT            | 24   | 4      | shooter_id, pos_x, pos_y, hit |
| GAME_OVER           | 25   | 1      | winner_id                     |

