
const Position = require("./position.js");
const {Message} = require("./message.js");
const utils = require("./utils/utils.js");

// callback functions for different message types
const protocol = [
  {
    id: 0,
    name: "SESSION_REFUSED",
    handler: (connection) => {
      console.log("There are already two players connected");
      connection.disconnect();
    },
  },
  {
    id: 1,
    name: "SESSION_WAITING",
    handler: (connection) => {
      console.log("You are connected, waiting for second player")
    },
  },
  {
    id: 2,
    name: "SESSION_STARTED",
    handler: (connection) => {
      console.log("Both parties are connected")
    },
  },
  {
    id: 3,
    name: "SESSION_CLOSED",
    handler: (connection) => {
      console.log("The session is over");
      process.exit();
    },
  },
  {
    id: 4,
    name: "SESSION_KEEP_ALIVE",
    handler: (connection) => {
      connection.sendMessage(new Message(utils.getTypeByName(protocol, "SESSION_STILL_ALIVE").id))
    },
  },
  {
    id: 5,
    name: "SESSION_STILL_ALIVE",
    handler: (connection) => {
      // console.log("still-alive")
      connection.connectionTimeout.refresh();
    },
  },
  {
    id: 6,
    name: "SESSION_STATUS",
    handler: (payload) => {
      console.log(payload);
    },
  },
  {
    id: 10,
    name: "MESSAGE_OK",
    handler: (connection) => {
      connection.event.emit('message_ok');
      // console.log("Message OK")
    },
  },
  {
    id: 11,
    name: "MESSAGE_BROKEN",
    handler: (connection) => {
      console.log("CRC is not the same");
      connection.event.emit('message_broken');
    },
  },
  {
    id: 12,
    name: "MESSAGE_RESET",
    handler: (connection) => { 
      connection.event.emit('message_broken');
    },
  },
  {
    id: 20,
    name: "GAME_INIT",
    handler: (connection, message) => {
      connection.event.emit('game_init', message.payload[0]);
    },
  },
  {
    id: 21,
    name: "GAME_START",
    handler: (connection, message) => {
      connection.event.emit('game_started', message.payload);
    },
  },
  {
    id: 22,
    name: "GAME_TURN",
    handler: (connection, message) => {
      connection.event.emit('game_turn', message.payload);
    },
  },
  {
    id: 23,
    name: "GAME_SHOOT",
    handler: (connection, message) => {
      let payload = message.payload;
      connection.event.emit('send_attack', payload[0], new Position(payload[1], payload[2]));
    },
  },
  {
    id: 24,
    name: "GAME_HIT",
    handler: (connection, message) => {
      let payload = message.payload;
      connection.event.emit('game_hit', payload[0], new Position(payload[1], payload[2]), payload[3], payload[4]);
    },
  },
  {
    id: 25,
    name: "GAME_OVER",
    handler: (connection, message) => {
      // console.log("Game Over, Winner: ", message.payload[0]);
      connection.event.emit('game_over', message.payload[0]);
    },
  },
];

module.exports = protocol;
