const BattleFieldCell = require('./battlefield_cell');

/**
 * describes a ship in the game
 */
class BattleShip {   

    length;
    isHorizontal;
    /**
     * cells occupied by this ship
     * @type { BattleFieldCell[] }
     */
    shipCells;

    /**
     * Creates a new BattleShip
     * @constructor
     * @param { Position } position - the starting {@link Position} of this ship (upper left)
     * @param { int } length - length of the ship/amount fo cells occupied
     * @param { bool } isHorizontal - whether is is horzontally orientated (true: right, false: down)
     */
    constructor(position, length = 2, isHorizontal = true) {
        this.length       = length;
        this.isHorizontal = isHorizontal;
        this.shipCells    = []

        this.init(position);
    }

    /**
     * registers all occupied cells of this ship iwthin the shipCells Array
     * @param { Position } position - the starting {@link Position} of this ship (upper left)
     */
    init(position) {
        for (let i = 0; i < this.length; i += 1) {
            const shipCell = new BattleFieldCell(position);

            if (this.isHorizontal) {
                shipCell.position.x += i
            }
            else {
                shipCell.position.y += i
            }

            this.shipCells.push(shipCell);
        }
    }

    /**
     * whether this ship is destroyed (all cells are hit )
     * @returns bool
     */
    isDestroyed() {
        for (const cell of this.shipCells) {
            if (!cell.isHit) {
                return false;
            }
        }

        return true;
    }
}

module.exports = BattleShip;