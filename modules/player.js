const Events = require('events');
const Connection = require('./connection.js');


/**
 * Player 
 */
class Player {
    id;
    connection;
    /**
     * Array of BattleFieldCells to keep track of the enemy field. Starts empty. On each attack a new cell is appended
     * @type { BattleFieldCell[] }
     */
    attackField;
    /**
     * Array of BattleShips for this player
     * @type { BattleShip[] }
     */
    defenseField;
    event;

    /**
     * Creates a new Player
     * @constructor
     * @param { int } id - the player id
     * @param { Connection } connection - Websocket {@link Connection} 
     */
    constructor(id, connection) {
        this.id = id;
        this.connection = new Connection(connection);
        this.attackField  = [];        
        this.defenseField = [];
        this.event = new Events.EventEmitter;
    }

    /**
     * emits 'attack_success' if attack successfull and sets the cell.isHit attribute accordingly
     * @param { Position } position - the {@link Position} of the attack
     */
    receiveAttack(position) {
        for (const ship of this.defenseField) {
            for (const cell of ship.shipCells) {
                if (!position.equals(cell.position)) continue;
                
                cell.isHit = true;

                return this.event.emit('attack_success', position, ship.isDestroyed());
            }
        }

        this.event.emit('attack_failed', position);
    }

    /**
     * returns true if all ships of this player are destroyed
     * @returns bool
     */
    isDefeated() {
        for (const ship of this.defenseField) {
            if (!ship.isDestroyed()) {
                return false;
            }
        }

        return true;
    }
}

Player.prototype.equals = function (other) {
    return this.id === other.id;
};

module.exports = Player;
