const Events = require('events');
const {
  Message
} = require('./message.js');
const utils = require('./utils/utils.js');
const protocol = require('./protocol.js');


/**
 * Represents a Connection to send and receive Messages through a websocket connection
 */
class Connection {
  connection;
  statistics;
  received = [];
  queue = [];
  lastMessage;
  event;
  keepAlive;
  connectionTimeout;
  messageTimeout;
  statusUpdate;
  /**
   * Create a connection object
   * @constructor
   * @param connection - Websocket Connection
   */
  constructor(connection) {
    this.statistics = {};
    this.connection = connection;
    this.event = new Events.EventEmitter;

    // handle every message that will be recieved on the connection
    this.connection.on('message', async (msg) => {
      var message = this.receiveMessage(msg);
      if (message.isValid()) {
        // handle the SESSION_STATUS message
        if (message.type == utils.getTypeByName(protocol, "SESSION_STATUS").id) {
          utils.getTypeById(protocol, message.type).handler(message.payload);
        }
        // handle all messages but the MESSAGE_OK
        else if (message.type != utils.getTypeByName(protocol, "MESSAGE_OK").id) {
          // send back a MESSAGE_OK to confirm recieved message
          this.sendMessage(new Message(utils.getTypeByName(protocol, "MESSAGE_OK").id));
          // push message to received -> to construct multiple packages into one
          this.received.push(message);

          var constructed = Message.fromPackages(this.received);
          // construct packages into message if the payload is equal to the defined length
          if (constructed.payload.length == constructed.length) {
            utils.getTypeById(protocol, message.type).handler(this, constructed);
            this.received = [];
          }
          // send a MESSAGE_BROKEN if the constructed message is to big
          else if (constructed.payload.length > constructed.length) {
            this.sendMessage(new Message(utils.getTypeByName(protocol, "MESSAGE_BROKEN").id));
            this.received = [];
          }
        }
        // send the next message from queue if a MESSAGE_OK is recieved
        // and clear the timeout for the resend functionality in "sendMessage"
        else {
          if (this.queue.length > 0)
            this.sendMessage(this.queue.shift());
          clearTimeout(this.messageTimeout);
        }
      }
      // send a MESSAGE_BROKEN if the message is invalid (wrong crc)
      else {
        this.sendMessage(new Message(utils.getTypeByName(protocol, "MESSAGE_BROKEN").id));
        this.received = [];
      }
    });
  }


  /**
   * Initialize keepalive and timeout for the connection
   * @param  keepAlive - Keep alive function handle
   * @param  timeout - Timeout function handle
   */
  startKeepAlive(keepAlive, timeout) {
    this.keepAlive = keepAlive();
    this.connectionTimeout = timeout();
  }

  /**
   * Initialize status updates for the connection
   * @param statusUpdate - Status update function handle
   */
  startStatusUpdate(statusUpdate) {
    this.statusUpdate = statusUpdate();
  }

  /**
   * Sends a message through the connection.
   * The message starts a timeout and will be send again after, if no message_ok resets the timeout.
   * @param {Message} message - {@link Message} object
   */
  sendMessage(message) {
    // timeout to resend message in case the server did not respond
    var ignore = ["MESSAGE_OK", "SESSION_KEEP_ALIVE", "SESSION_STILL_ALIVE", "SESSION_STATUS"];
    if (!(ignore.includes(utils.getTypeById(protocol, message.type).name))) {
      clearTimeout(this.messageTimeout)
      this.messageTimeout = setTimeout(() => {
        console.log("RESET CONNECTION");
        this.sendMessage(message);
      }, 5000);
    }
    this.connection.sendBytes(message.toBuffer());
  }

  /**
   * Splits a message into multiple packages and adds them to the queue
   * @param {Message} message - {@link Message} object
   */
  queueMessage(message) {
    if (!(message instanceof Message)) {
      throw new Error(`Input has to be of type Message, but type ${typeof (message)} given instead!`);
    }
    // save the message as last message, in case the send message is MESSAGE_BROKEN
    this.lastMessage = message;

    var packages = message.toPackages();
    // add every package to the queue, so it will be send one by one
    packages.forEach((message) => {
      if (message === undefined) {
        throw new Error("Package is undefined!")
      }
      this.queue.push(message)
    });
    this.saveStatistics(message, true);
  }

  /**
   * Recieve a message in the connection and transorm it into a Message Object
   * @param {buffer} message - {}
   * @returns {Message} message - {@link Message} object
   */
  receiveMessage(message) {
    const buffer = message.binaryData;
    var message = Message.fromBuffer(buffer);
    this.saveStatistics(message, false);
    return message;
  }

  /**
   * Add a message with the sent or recieve information to the statistics
   * Creates a JSON Object or increases the related number
   * @param {Message} message - {@link Message} object
   * @param {bool} sent - sent or recieve statistic
   */
  saveStatistics(message, sent) {
    var type = utils.getTypeById(protocol, message.type).name;
    var key = sent ? "sent" : "received";

    // create json object if not exists
    if (!(type in this.statistics))
      this.statistics[type] = {
        "sent": 0,
        "received": 0
      };

    // increase count for defined type (received/sent)
    this.statistics[type][key] = this.statistics[type][key] + 1;
  }

  /**
   * disconnect the connection from the server and cleanup all intervalls/timeouts
   * [keepAlive, statusUpdate, connectionTimeout, messageTimeout]
   */
  disconnect() {
    console.log("disconnected");
    clearInterval(this.keepAlive);
    clearInterval(this.statusUpdate);
    clearTimeout(this.connectionTimeout);
    clearTimeout(this.messageTimeout);
    this.connection.close();
  }
}

module.exports = Connection;