const expect = require('chai').expect;
const Position = require('../position');

describe('position.js', function () {

    describe ('#equals', function () {
        it ('Identical Positions should evaluate to be equal' , function () {
            const pos1 = new Position(1, 1);
            const pos2 = new Position(1, 1);
            expect(pos1.equals(pos2)).to.be.true;
        });
        it ('Non-identical Positions should evaluate to be inequal' , function () {
            const pos1 = new Position(1, 1);
            const pos2 = new Position(2, 5);
            expect(pos1.equals(pos2)).to.be.false;
        });
    });
});