var assert = require('chai').assert;
var expect = require('chai').expect;
var u = require('../utils/utils.js');

const map = [
    {
      id: 0,
      name: "SESSION_REFUSED",
      handler: () => console.log("Not Implemented"),
    },
    {
      id: 1,
      name: "SESSION_WAITING",
      handler: () => console.log("Not Implemented"),
    },
    {
      id: 2,
      name: "SESSION_STARTED",
      handler: () => console.log("Not Implemented"),
    }
  ];

describe('utils.js', function(){
  describe('#getTypeByKey(object, key, keyValue)', function(){
      map.forEach(({id, name})=> {
          it(`id = ${id} should be ${name}`, function(){
              var obj = u.getTypeById(map, id);
              expect(obj.name == name).to.be.true;
          })
          it(`${name} should have id ${id}`, function(){
              var obj = u.getTypeByName(map, name);
              expect(obj.id == id).to.be.true;
          })
      })
  })
})
