var assert = require('chai').assert;
var expect = require('chai').expect;
var protocol = require('../protocol.js');
var {Message, MAXIMUM_PAYLOAD_BYTES} = require('../message.js');


describe('protocol.js', function () {
    describe('Message', function () {
        var testMessages = [
            { type:  0, payload: [], expectedLength: 3 },
            { type: 20, payload: [1], expectedLength: 4 },
            { type: 12, payload: [2, 2], expectedLength: 5 },
            { type: 14, payload: [2, 2, 1], expectedLength: 6 },
            { type: 14, payload: [2, 2, 1, 9], expectedLength: 7 },
        ]
        it('Valid type, empty payload [] should return Message Object', function () {
            var message = new Message(type = 1, payload = []);
            assert.equal(message.length, payload.length, "length to equal to payload length");
            assert.instanceOf(message, Message, "message is instance of Message");
        }),
        it('Valid type, empty payload [] should return Message Object', function () {
            var message = new Message(type = 1, payload = [], length = 20);
            assert.equal(message.length, length, "length to equal to given length");
            assert.instanceOf(message, Message, "message is instance of Message");
        }),
            it('Invalid type should raise an exception', function () {
                expect(() => { new Message(type = "error", payload = []) }).to.throw();
            }),
            it('Invalid payload should raise an exception', function () {
                expect(() => { new Message(type = 0, payload = "error") }).to.throw();
            }),
            it('Valid type and payload should return Message object', function () {
                var message = new Message(type = 11, payload = []);
                assert.instanceOf(message, Message, "message is instance of Message");
            })

        describe('#toPackages()', function () {
            testMessages.forEach(({type, payload }) => {
                var message = new Message(type, payload);
                var expectedMessageCount = Math.max(1, Math.ceil(message.payload.length /MAXIMUM_PAYLOAD_BYTES));
                var messages = message.toPackages();
                var totalPayloadLength = 0;
                messages.forEach((message) => {
                    totalPayloadLength += message.payload.length;
                })
                it(`Split message into ${expectedMessageCount} packages`, function () {
                    assert.equal(messages.length, expectedMessageCount);
                })
                it(`Total payload length ${totalPayloadLength} should be equal to message.length ${message.length}`, function () {
                    assert.equal(message.length, totalPayloadLength);
                })
            })
        })
        describe('#fromPackages()', function () {
            testMessages.forEach(({type, payload }) => {
                var message = new Message(type, payload);
                var messages = message.toPackages();
                var messageFromPackages = Message.fromPackages(messages);
                it(`Message from packages should be of instance Message`, function () {
                    assert.instanceOf(message, Message);
                })
                it(`Message from packages should be equal to original message`, function () {
                    expect(message.equals(messageFromPackages)).to.be.true;
                })
            })
        })
        describe('#toBuffer()', function () {
            testMessages.forEach(({ type, payload, expectedLength }) => {
                var message = new Message(type, payload);
                var buffer = message.toBuffer();
                it(`Type: ${type}, Payload: [${payload}], should have length ${expectedLength}`, function () {
                    assert.equal(buffer.length, expectedLength);
                })
                it(`Type: ${type}, Payload: [${payload}], should be of type Buffer`, function () {
                    assert.instanceOf(buffer, Buffer);
                })
            })
        })
        describe('#fromBuffer(buffer)', function () {
            testMessages.forEach(({ type, payload }) => {
                var message = new Message(type, payload);
                var buffer = message.toBuffer();
                it(`Type: ${type}, Payload:[${payload}], should be of type Message`, function () {
                    assert.instanceOf(Message.fromBuffer(buffer), Message);
                })
            })
        })
        describe('#equals(o)', function () {
            testMessages.forEach(({ type, payload }) => {
                var message1 = new Message(type, payload);
                var message2 = new Message(type, payload);
                it(`${message1.toString()} == ${message2.toString()} is equal`, function () {
                    expect(message1.type == message2.type).to.be.true;
                    expect(message1.length == message2.length).to.be.true;
                    expect(!Math.abs(Buffer.compare(message1.payload, message2.payload))).to.be.true;
                    expect(message1.crc == message2.crc).to.be.true;
                })
            })
        })
    })
})