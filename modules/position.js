/**
 * Position (coordinate) in a game field
 */
class Position {

    x;
    y;

    /**
     * Creates a new Position
     * @constructor
     * @param { int } x
     * @param { int } y
     */
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }
}

// creates a random Position 
Position.random = function (width, height) {
    const x = Math.floor(Math.random() * Math.floor(width));
    const y = Math.floor(Math.random() * Math.floor(height));
    return new Position(x, y);
};

Position.prototype.equals = function (other) {
    return this.x === other.x && this.y === other.y;
};
Position.prototype.toString = function () {
    return `(${this.x}, ${this.y})`;
};

module.exports = Position;