const Position = require('./position');

/*
BattleFieldCell:
 - describes a cell in game
@Attributes:
 - position: position of the cell
 - isHit: whether the cell was attacked
*/

/**
 * describes a cell in the game
 */
class BattleFieldCell {

    position;
    isHit;

    /**
     * Creates a new BattleFieldCell
     * @constructor
     * @param { Position } position -  the {@link Position} (coordinate) of the cell
     * @param { bool } isHit - whether this cell got attacked
     */
    constructor(position, isHit = false) {
        this.position = new Position(position.x, position.y);
        this.isHit    = isHit;
    }
}

module.exports = BattleFieldCell;