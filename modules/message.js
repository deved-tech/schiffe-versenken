const crc = require('./utils/checksum.js');

const TYPE_BYTES = 1;
const LENGTH_BYTES = 1;
const CRC_BYTES = 1;
const MAXIMUM_PAYLOAD_BYTES = 10;

/**
 * Represents a message object containing a type, a payload length, a payload and a crc

 */
class Message {
  type;
  length; 
  payload;
  crc8;

  /**
   * Create a message object, that can be send and received
   * The length of the payload will either be calculated or set
   * @constructor
   * @param {int} type - Message type
   * @param {Array} payload - Payload
   * @param {int} length - Payload length (<0) = calculated
   */
  constructor(type = -1, payload = [], length = -1) {
    if (!(typeof type == "number" && Number.isInteger(type)))
      throw new Error();
    if (!Array.isArray(payload))
      throw new Error();
  
    this.type = type;
    // length will be evaluated if no length not given ( < 0)
    this.length = length < 0 ? payload.length : length;
    this.payload = new Uint8Array(payload);
    this.crc8 = crc.checksum(payload);
  }

  /**
   * Split one message into multiple packages (split with MAXIMUM_PAYLOAD_BYTES)
   * @returns Message[]
   */
  toPackages() {
    if (this.length <= 1) {
      return [this];
    } 
    else {
      var messageCount = Math.max(1, Math.ceil(this.payload.length / MAXIMUM_PAYLOAD_BYTES));
      var messages = [];
      for (let idx = 0; idx < messageCount; idx++) {
        messages.push(new Message(this.type, Array.from(this.payload).slice(idx*MAXIMUM_PAYLOAD_BYTES, (idx+1)*MAXIMUM_PAYLOAD_BYTES) , this.length));
      }
      return messages;
    }
  }

  /**
   * Create one message from multiple messages
   * @param {Message[]} messages - packages
   * @returns Message
   */
  static fromPackages(messages) {
    if (!Array.isArray(messages))
      throw new Error();

    var payload = []
    messages.forEach((message) => {
      payload.push(Array.from(message.payload));
    })
    return new Message(messages[0].type, payload.flat(), messages[0].length);
  }

  /**
   * Convert message object to buffer
   * @returns Buffer
   */
  toBuffer() {
    var bytes = TYPE_BYTES + LENGTH_BYTES + this.payload.length + CRC_BYTES;
    var byteArray = new Uint8Array(bytes);
    byteArray.fill(this.type, 0);
    byteArray.fill(this.length, TYPE_BYTES);
    byteArray.set(this.payload, TYPE_BYTES + LENGTH_BYTES);
    byteArray.fill(this.crc8, bytes - CRC_BYTES);
    return Buffer.from(byteArray);
  }

  /**
   * Convert Buffer to message object
   * @param {Buffer} buffer - buffer to create message
   * @returns Message
   */
  static fromBuffer(buffer) {
    var message = new Message();
    const byteArray = new Uint8Array(
      buffer.buffer,
      buffer.byteOffset,
      buffer.byteLength / Uint8Array.BYTES_PER_ELEMENT
    );
    message.type = byteArray[0];
    message.length = byteArray[1];
    message.payload = byteArray.slice(2, byteArray.length - 1);
    message.crc8 = byteArray[byteArray.length - 1];
    return message;
  }

  /**
   * Convert messag object to string
   * @returns String
   */
  toString() {
    return `Message: { type: ${this.type}, length: ${this.length}, payload: [${this.payload}], crc: ${this.crc8} }`;
  }

  /**
   * Compare two messages and return if they are equal
   * @param {Message} o - message object
   * @returns bool
   */
  equals(o) {
    return (
      this.type == o.type &&
      this.length == o.length &&
      !Buffer.compare(this.payload, o.payload) &&
      this.crc == o.crc
    );
  }

  /**
   * Compare the set crc8 and calculate it again for the payload, return if they are equal
   * @returns bool
   */
  isValid() {
    return crc.checksum(this.payload) === this.crc8;
  }
}

module.exports = {MAXIMUM_PAYLOAD_BYTES, Message};
module.exports.MAXIMUM_PAYLOAD_BYTES = MAXIMUM_PAYLOAD_BYTES;
