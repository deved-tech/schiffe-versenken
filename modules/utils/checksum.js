const crc = require("crc");

/**
 * @fileOverview Bridge between crc module and message crc
 * @module checksum
 * @requires crc
 */

/**
 * Calculates checksum for message payload
 * @param {Array} payload 
 * @returns {uint8}
 */
function checksum(payload){
    return crc.crc8(payload);
}

module.exports = {checksum: checksum};