/**
 * @fileOverview Common utilities for the client consule interface
 * @module console_utils
 */

/**
 * visualizes a 2D array in a fancy and overengineered way
 * @param { int } size - size of the 2D array. Must be quadratic (x*x)
 * @param { Function } modifyMatrixFunction - the function which sets the output values of the array
 * @returns string
 */
function stringifyMatrix(size, modifyMatrixFunction) {
    // match unix styled coloring
    const regexp  = /\x1b\[\d\dm(.*?)\x1b\[0m/;
    // padding between cells
    const padding = 2;

    const array = Array.from(Array(size), () => '.'.repeat(size).split(''));      
    let string  = ''; 
    
    // this function will set the values
    modifyMatrixFunction(array);

    // turn array into a string
    for (let i = 0; i < size; i++) {                
        for (let j = 0; j < size; j++) {
            let cellString = `${array[i][j]}`;
            // pad string
            if (regexp.test(cellString)) {
                // string has unix styled coloring
                var match  = regexp.exec(cellString);
                cellString = ' '.repeat(padding) + cellString;
                cellString = cellString.substr(match[1].length, cellString.length);
            }
            else {
                // string is plain text
                cellString = ' '.repeat(padding) + cellString;
                cellString = cellString.substr(cellString.length - padding, cellString.length);
            }
            string += `${cellString} `; 
        }
        string += '\n';
    }

    return string;
}

/**
 * colorizes the output in a console
 */
const output = {
    black:      (text) => output.applyColor(30, text),
    red:        (text) => output.applyColor(31, text),
    green:      (text) => output.applyColor(32, text),
    yellow:     (text) => output.applyColor(33, text),
    blue:       (text) => output.applyColor(34, text),
    magenta:    (text) => output.applyColor(35, text),
    cyan:       (text) => output.applyColor(36, text),
    white:      (text) => output.applyColor(37, text),
    applyColor: (color, text) => `\x1b[${color}m${text}\x1b[0m`
};

module.exports = {
    output: output,
    stringifyMatrix: stringifyMatrix
}