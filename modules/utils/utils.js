/**
 * @fileOverview Functions to filter a json object

 * @module utils
 */


/**
 * Get type from map by comparing a key
 * @param {object} object - map with all message types 
 * @param {string} key - selected key
 * @param keyValue - value to compare
 * @returns object - type object
 */
function getTypeByKey(object, key, keyValue) {
  return object.filter((jsonObject) => {
    return jsonObject[key] == keyValue;
  })[0];
}

/**
 * Get type from map by comparing the id
 * @param {object} object - map with all message types 
 * @param id - value to compare
 * @returns object - type object
 */
function getTypeById(object, id) {
  return getTypeByKey(object, 'id', id)
}

/**
 * Get type from map by comparing the name
 * @param {object} object - map with all message types 
 * @param name - value to compare
 * @returns object - type object
 */
function getTypeByName(object, name) {
  return getTypeByKey(object, 'name', name)
}


module.exports = {getTypeById: getTypeById, getTypeByName: getTypeByName};