const fs = require('fs');
const path = require('path');

/**
 * @fileOverview Bridge between fs and save file
 * @module filehandler
 * @requires fs
 * @requires path
 */

/**
 * Write content to file defined in the filepath
 * @param {string} filepath - path to the file
 * @param {string} content - content of the file
 */
function writeToFile(filepath, content){
    if (!fs.existsSync(path.dirname(filepath))){
        fs.mkdirSync(path.dirname(filepath));
    }
    fs.writeFileSync(filepath, content);
}

module.exports = {writeToFile: writeToFile};
