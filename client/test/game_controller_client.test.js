const expect = require('chai').expect;


const GameControllerClient = require('../game_controller_client').GameControllerClient;
const Player = require('../../modules/player');

const fakeConnection = { on() {} };
const gameController = new GameControllerClient(fakeConnection);

const field1D = [
    0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
    0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0,
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1,
    1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0
]

describe('game_controller_sever.js', function () {

    describe('#deserializeDefenseField()', function () {
        it('deserialized defensefield should have as many cells as the 1D defensefield', function () {
            // count the amount of ships, that should be generated
            const field = gameController.deserializeDefenseField(field1D);
            expect(field.length * field[0].length === field1D.length).to.be.true;
        });
    });
});