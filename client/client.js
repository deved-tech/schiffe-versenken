var WebSocketClient = require('websocket').client;
var Message = require('../modules/protocol.js')
const GameControllerClient = require('./game_controller_client.js').GameControllerClient;
const prompt = require('prompt-async');


const DEFAULT_SERVER_ADDRESS = "ws://localhost:8080/";

var gameControllerClient;
const client = new WebSocketClient();

client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {
    gameControllerClient = new GameControllerClient(connection);
    console.log('WebSocket Client Connected');

    connection.on('error', function(error) {
        console.log("Connection Error: " + error.toString());
    });
    
    connection.on('close', function() {
        console.log('echo-protocol Connection Closed');
    });
});

async function connectToServer(){
    const text  = `Server address [${DEFAULT_SERVER_ADDRESS}]`;
    try{
        let address = await prompt.get(text);
        console.log(address[text]);
        address = address[text] ? address[text] : DEFAULT_SERVER_ADDRESS;
        client.connect(address, 'echo-protocol');
    }catch{
        console.log("\nTo connect, try again!");
    }
}

connectToServer();

