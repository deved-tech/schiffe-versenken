const prompt   = require('prompt-async');
const readline = require('readline');
const Position = require('../modules/position');
const consoleUtils = require('../modules/utils/console_utils');

const blocks = new Map(); 
var linesWritten = 0;


/**
 * interface to nicely print the game to the client in the console
 */
class ConsoleInterface {

    playerID;
    defenseField;
    attackField;

    constructor() {}

    /**
     * Initializes the console interface and visualizes the game. Must be called on game_started
     * @param { int } playerID
     * @param { int[][] } defenseField
     * @param { int[][] } attackField
     */
    onGameStart(playerID, defenseField, attackField) {
        // init the ui
        this.playerID = playerID;
        this.defenseField = defenseField;
        this.attackField  = attackField;

        // visualize
        this.display('title', '');
        this.displayGameFields(defenseField, attackField);
        this.display('info',  'Waiting for attack...');
        this.display('input', '');
    }

    /**
     * Visualizes the new turn. Must be called on game_turn
     * @param { int } id - id of active player
     */
    onGameTurn(id) {
        this.display('title', id == this.playerID ? 'It\'s your turn!' : 'It\'s the enemies turn!');
        this.displayGameFields();
        this.display('input', '');
    }

    /**
     * Visualizes a game hit. Must be called on game_hit
     * @param { int } id - id of active player
     * @param { Position } position - the {@link Position } attacked
     * @param { bool } success - whether a ship cell got hit
     * @param { bool } isShipDestroyed - whether a whole ship got destroyed
     */
    onGameHit(id, position, success, isShipDestroyed) {
        // construct the message for the user
        let message = id === this.playerID ? 'Your' : 'Enemy';
        message    += ` attack was ${success ? '' : 'un'}successfull at ${position}`;
        message    += isShipDestroyed ? `. A ship got destroyed!` : '';

        this.displayGameFields();
        this.display('info', message);
    }

    /**
     * Visualizes the game over state. Must be called on game_over
     * @param { int } winnerId - id of the winning player
     * @param { Function } onExit - the function to call to finish the game/ui
     */
    onGameOver(winnerId, onExit) {
        this.display('title', 'Game Over!');
        this.display('info', `${winnerId === this.playerID ? 'You' : 'The enemy'} won!`);
        this.display('input', '');

        prompt.get('press any key to disconnect...')
            .catch(error => undefined) // in case user interrupts prompt
            .finally(() => onExit());
    }
        
    /**
     * Visualizer for the console
     * @param { string } type - the type of information that will be displayed (eg. title)
     * @param { string } text - the inforamtion to display. Will replace the old information block
     */
    display(type, text) {
        // retrieve the number of lines the block will write
        const lines = text.split('\n').length + 1;
    
        // block has not been written yet, so just append the text to the console
        if (!blocks.has(type)) {
            // set the start position to the current cursor position
            const start = linesWritten;
    
            // update the line counter
            linesWritten += lines;
    
            // update the map
            blocks.set(type, { text: text, lines: lines, start: start });
    
            // write the new block
            console.log(text);
            console.log();
    
            return;
        }
    
        // retrieve the old block
        const oldBlock = blocks.get(type);
    
        // set the start position of the new block to the start position of the old block
        const start = oldBlock.start;
    
        // move the cursor to the beginning of the old block
        readline.moveCursor(process.stdout, 0, -(linesWritten - oldBlock.start));
    
        // clear each line of the old block
        for (let i = 0; i < oldBlock.lines; i++) {
            readline.clearLine(process.stdout, 0);
            readline.moveCursor(process.stdout, 0, 1);
        }
    
        // move the cursor to the beginning of the old block
        readline.moveCursor(process.stdout, 0, -oldBlock.lines);    
        // write the new block
        console.log(text);
        console.log();
    
        // update the line counter
        linesWritten -= oldBlock.lines;
        linesWritten += lines;
    
        // move the cursor to the end of the console
        readline.moveCursor(process.stdout, 0, (linesWritten - start - lines));
    
        // update the map
        blocks.set(type, { text: text, lines: lines, start: start });
    }

    /**
     * displays both the defensefield and attackfield side by side
     */
    displayGameFields() {
        let string = visualizeFieldsInline(this.defenseField, this.attackField);
        this.display('fields', string);
    }

    /**
     * retrieves the coordinate to attack from the player
     * @param { Position | undefined } lastAttackedPosition - the {@link Position } of the old attack (only if the last input was invalid) 
     * @returns Position
     */
    async retrieveAttackPosition(lastAttackedPosition = undefined) {
        const fieldSize = this.defenseField.length;
        let message = '';
        if (lastAttackedPosition !== undefined) {
            message += `Position ${lastAttackedPosition} already attacked! `;
        }
        message += `Expecting numbers between ${0} and ${fieldSize - 1}:`;
        
        this.display('input', message);

        let row = await inputToNumber('Enter the row to attack', 0, fieldSize - 1);
        let col = await inputToNumber('Enter the column to attack', 0, fieldSize - 1);

        clearInput();

        return new Position(row, col);
    }
}

module.exports = ConsoleInterface;

// private helper functions

// visualizes both fields side by side
function visualizeFieldsInline(defenseField, attackField) {
    const fieldSize = defenseField.length;
    const stringlistLeft  = visualizeField(defenseField).split('\n').filter(element => element != '');
    const stringlistRight = visualizeField(attackField).split('\n').filter(element => element != '');

    const stringlistLength = Math.min(stringlistLeft.length, stringlistRight.length);

    const padding = (fieldSize + 1) * 3;
    
    const titleLeft  = 'Defensefield:' + ' '.repeat(padding);
    const titleRight = 'Attackfield:'  + ' '.repeat(padding);
    const sperator   = '  |  ';
    let mergedString = titleLeft.substr(0, padding) + sperator + titleRight.substr(0, padding);

    for (let i = 0; i < stringlistLength; i++) {
        mergedString += '\n' + stringlistLeft.shift() + sperator + stringlistRight.shift();
    }
    
    return mergedString;
}

// turns a 2D array in a fancy string
function visualizeField(field) {    
    const fieldSize = field.length + 1;

    const modifyMatrix = function (array) {
        for (let i = 0; i < fieldSize; i++) {       
            for (let j = 0; j < fieldSize; j++) {

                if (i === 0 && j === 0) {
                    array[i][j] = ' ';
                    continue
                }
                if (i === 0) {
                    array[i][j] = consoleUtils.output.yellow(j-1);
                    // array[i][j] = j;
                    continue;
                }       
                if (j === 0) {
                    array[i][j] = consoleUtils.output.yellow(i-1);
                    // array[i][j] = i;
                    continue;
                }     

                let value = field[i-1][j-1];
                
                switch(value) {
                    case 0: // defenseField: unattacked water | attackField: unattacked cell
                        value = '.';
                        break;
                    case 1: // defenseField: unattacked ship  | attackField: not possible!
                        value = 'o';
                        break;
                    case 2: // attacked water
                        value = consoleUtils.output.red('x');
                        break;
                    case 3: // attacked ship
                        value = consoleUtils.output.red('o');
                        break;
                    case 4: // defeated ship
                        value = consoleUtils.output.magenta('o'); // sexy magenta
                        break;
                    default:
                        console.warn(`invalid value ${value} in defensefield of player ${this.playerID}`);
                }

                array[i][j] = value;
            }
        }
    };

    return consoleUtils.stringifyMatrix(fieldSize, modifyMatrix);
}

// to request the row and column
async function inputToNumber(text, lower, upper) {
    let number; 
    linesWritten++;

    prompt.start();

    do {
        // number is not undefined -> it was already set, but was invalid
        if (number !== undefined) {
            // move cursor up
            readline.moveCursor(process.stdout, 0, -1);
            // and clear line
            readline.clearLine(process.stdout, 0);
        }

        let input = await prompt.get(text);
        number = parseInt(input[text]);

    } while (isNaN(number) || number < lower || number > upper);

    return number;
}

// clears the input
function clearInput() {   
    //clears the lat two lines of input
    linesWritten -= 2;
    
    for (var i = 0; i < 2; i++) {
        readline.moveCursor(process.stdout, 0, -1);
        readline.clearLine(process.stdout, 0);
    }
}