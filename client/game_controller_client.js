const ConsoleInterface = require('./console_interface');

const protocol   = require('../modules/protocol.js');
const Connection = require('../modules/connection.js');
const {Message}  = require('../modules/message.js');
const protocolUtils = require('../modules/utils/utils.js');


/**
 * Represents a the game controller on clients side
 */
class GameControllerClient {    
    /**
    * id of the player associated 
    * @type { int }
    */
    playerID;
    connection;
    /**
     * where 0 = water, 2 = hit water, 3 = hit ship, 4 = destroyed ship
     * @type { int[][] }
     */
    attackField; 
    /**
    * where 0 = water, 1 = ship, 2 = hit water, 3 = hit ship, 4 = destroyed ship
    * @type { int[][] }
    */
    defenseField;
    ui;

    /**
     * Creates a client game controller
     * @constructor
     * @param { Connection } connection - Websocket {@link Connection} 
     * @param { UserInterface } ui - Unser Interface for game visualisation
     * @default { ConsoleInterface}
     */
    constructor(connection, ui = new ConsoleInterface()) {
        this.connection = new Connection(connection);
        this.ui = ui;
        this.initEvents();
    }

    /**
     * initializes the event hanlders for game_init, game_started, game_turn, game_hit, game_over
     */
    initEvents () {
        this.connection.event.on('game_init', (id) => {
            this.playerID = id;
        });

        this.connection.event.on('game_started', (defenseField1D) => {
            // construct the defense- and attackfield
            this.defenseField = this.deserializeDefenseField(defenseField1D);
            const fieldSize   = this.defenseField.length;
            this.attackField  = Array.from(Array(fieldSize), () => new Array(fieldSize).fill(0));
            
            // init the ui
            this.ui.onGameStart(this.playerID, this.defenseField, this.attackField);
        });

        this.connection.event.on('game_turn', (id) => {
            this.ui.onGameTurn(id);

            if (id == this.playerID) {
                this.play();
            }
        });

        this.connection.event.on('game_hit', (id, position, success, isShipDestroyed) => {
            // the field to modify
            const field = id === this.playerID ? this.attackField : this.defenseField;
            // set the field accordingly
            field[position.x][position.y] = success ? 3 : 2;

            if (isShipDestroyed) {   
                // mark the ship as destroyed
                this.setShipCellsDestroyed(field, position);
            }

            this.ui.onGameHit(id, position, success, isShipDestroyed);
        });

        this.connection.event.on('game_over', (winnerId) => {
            this.ui.onGameOver(winnerId, () => this.connection.disconnect());
        });
    }

    /**
     * waites for the user input provided by the ui object
     */
    async play() {                
        let attackPosition;

        do {
            attackPosition = await this.ui.retrieveAttackPosition(attackPosition);
        }
        while (!this.isAttackPositionValid(attackPosition));

        this.sendAttack(attackPosition);
    }

    /**
     * checks if a position is valid to attack (eg. was not attacked before)
     * @param { Position } attackPosition - {@link Position} to check
     * @returns bool
     */
    isAttackPositionValid(attackPosition) {
        return this.attackField[attackPosition.x][attackPosition.y] === 0
    }

    /**
     * sends an attack event to the server
     * @param { Position } attackPosition - {@link Position} to attack
     */
    sendAttack(attackPosition) {
        const payload = [this.playerID, attackPosition.x, attackPosition.y];
        this.connection.sendMessage(new Message(protocolUtils.getTypeByName(protocol, 'GAME_SHOOT').id, payload));
    }

    /**
     * sets all occupied cells of a destroyed ship to the value 4 in a field (= destroyed)
     * @param { int[][] } field - defensefield or attackfield
     * @param { Position } position - last {@link Position} of a ship that was destroyed
     */
    setShipCellsDestroyed(field, position) {
        field[position.x][position.y] = 4;

        // helper function to iterate the cells in a direction relative to the position
        // and marking them as destroyed if they belong to a ship (no water cell in between)
        const setCellsDestroyed = function (dx, dy) {
            let i = position.x + dx;
            let j = position.y + dy;
            for (; 0 <= i && i < field.length && 
                   0 <= j && j < field.length;
                 i += dx, j += dy) {
                // is cell not part of a ship -> break
                if (field[i][j] < 3) break;
                field[i][j] = 4;
            }
        };

        setCellsDestroyed( 1, 0); // positive rows relative to position
        setCellsDestroyed(-1, 0); // negative rows
        setCellsDestroyed( 0, 1); // positive columns
        setCellsDestroyed( 0,-1); // negative columns
    }

    /**
     * deserializes the received 1D defensefield into a 2D Array
     * @param { Uint8Array } array - 1D defensefield
     * @returns int[][]
     */
    deserializeDefenseField(array) {
        const fieldSize  = array.length**(0.5)
        let defenseField = [];

        for (let i = 0; i < fieldSize; i++) {
            defenseField.push(array.slice(i * fieldSize, (i + 1) * fieldSize));
        }

        return defenseField;
    }
}

module.exports.GameControllerClient = GameControllerClient;