var WebSocketServer = require('websocket').server;
var http = require('http');
const GameControllerServer = require('./game_controller_server.js').GameControllerServer;
const protocol = require('../modules/protocol.js');
const {
    Message
} = require('../modules/message.js');
const utils = require('../modules/utils/utils.js');
const writeToFile = require('../modules/utils/filehandler.js').writeToFile;

const PORT = 8080;
const KEEPALIVE_INTERVAL = 4000;
const TIMEOUT = 5000;

var gameControllerServer;
var stats = {};

const server = http.createServer(function (request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});

server.listen(PORT, function () {
    gameControllerServer = new GameControllerServer();
    console.log((new Date()) + ' Server is listening on port 8080');
});

wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false,
    closeTimeout: 1000,
    keepalive: false
});

wsServer.on('request', function (request) {
    const wsConnection = request.accept('echo-protocol', request.origin);
    const player = gameControllerServer.addPlayer(wsConnection);

    if (gameControllerServer.players.length <= 2) {
        // keep alive interval, send the keep alive message
        const keepAlive = () => setInterval(() => {
            player.connection.sendMessage(new Message(utils.getTypeByName(protocol, "SESSION_KEEP_ALIVE").id));
        }, KEEPALIVE_INTERVAL);

        // timeout, disconnect the client when keep alive answer does not come in timeout 
        const timeout = () => setTimeout(() => {
            console.log("DISCONNECT DUE TO TIMEOUT");
            player.connection.sendMessage(new Message(utils.getTypeByName(protocol, "SESSION_CLOSED").id));
            player.connection.disconnect();
        }, TIMEOUT);

        // initialize keep alive and timeout
        player.connection.startKeepAlive(keepAlive, timeout);

    }
    wsConnection.on('close', function (reasonCode, description) {
        console.log((new Date()) + ' Peer ' + wsConnection.remoteAddress + ' disconnected.');

        if (wsServer.connections.length < 2) {
            // disconnect the clients
            gameControllerServer.players.forEach((player) => {
                stats[`Player ${player.id}`] = player.connection.statistics;
                player.connection.sendMessage(new Message(utils.getTypeByName(protocol, "SESSION_CLOSED").id))
                player.connection.disconnect();
            })

            // safe statistics
            if (gameControllerServer.players.length == 2) {
                const filepath = `./stats/stats-${new Date().getTime()}.json`
                writeToFile(filepath, JSON.stringify(stats));
            }
            gameControllerServer.players = [];
        }else{
            gameControllerServer.players.pop();
        }
    });
});