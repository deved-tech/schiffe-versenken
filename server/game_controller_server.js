const Player = require('../modules/player');
const Position = require('../modules/position');
const Battleship = require('../modules/battleship');
const BattleFieldCell = require('../modules/battlefield_cell');

const protocol = require('../modules/protocol.js');
const {Message} = require('../modules/message.js');
const utils = require('../modules/utils/utils.js')

/**
 * the game controller on the backend
 */
class GameControllerServer {    
    /**
    * Array of two players
    * @type { Player[] }
    */
    players;
    /**
    * active players
    * @type { Player }
    */
    activePlayer;
    fieldSize;
    createBattleShipMaxIterations;
    shipSizes = [
        { length: 2, amount: 3 },
        { length: 3, amount: 2 },
        { length: 4, amount: 1 },
    ];

    /**
     * Creates a server game controller
     * @constructor
     * @param { int } fieldSize - the length of a game field egde (will return in a quadratic game field)
     * @param { Array } shipSizes - defines the amount of ships within the game (JSON Array of JSON Objects that contain length and amount properties)
     */
    constructor(fieldSize = 10, shipSizes = this.shipSizes) {
        this.fieldSize = fieldSize;
        this.shipSizes = shipSizes;
        this.createBattleShipMaxIterations = fieldSize ** 2 * 2;
        this.players = [];
    }
    
    /**
     * initializes the game controller
     */
    init() {
        // create the game fields for each player
        try {        
            for (const player of this.players) {
                this.setBattleShips(player);
            }
        }
        // if a ship creation for player failed reduce the amount of ships and try again :)
        catch (error) {
            console.log(`BattleShip creation failed!\n${error}\nReducing the amount of all ships...`);

            for (let i = 0; i < this.shipSizes.length; i++) {
                if (this.shipSizes[i].amount === 0) continue;
                this.shipSizes[i].amount--;
            }
            // clear the old player defenseField
            for (const player of this.players) {
                player.defenseField = [];
            }
            return this.init();
        }

        this.initEvents(this.players[0], this.players[1]);
        this.initEvents(this.players[1], this.players[0]);  

        // start game
        for (let player of this.players) {
            const payload = this.serializeDefenseField(player);
            player.connection.queueMessage(new Message(utils.getTypeByName(protocol, "GAME_START").id, payload))
        }

        // select active player
        this.activePlayer = this.players[1];
        this.swapActivePlayer();        
    }

    /**
     * initializes the event hanlders for send_attack, attack_success, attack_failed
     * @param {Player} player - the one player
     * @param {Player} enemy - the other player
     */
    initEvents(player, enemy) {
        player.connection.event.on('send_attack', (id, position) => {
            console.log(`player ${id} is attacking position ${position}...`);

            enemy.receiveAttack(position);
        });

        player.event.on('attack_success', (position, isShipDestroyed) => {
            console.log(`attack succeeded! ${isShipDestroyed ? 'A ship got destroyed!':''}`);

            enemy.attackField.push(new BattleFieldCell(position, true));

            const payload = [enemy.id, position.x, position.y, 1, isShipDestroyed];
            this.players.forEach((player) => player.connection.queueMessage(new Message(utils.getTypeByName(protocol, "GAME_HIT").id, payload)));
            this.gameTurn();
        });

        player.event.on('attack_failed', (position) => {
            console.log('attack failed!');

            enemy.attackField.push(new BattleFieldCell(position, false));

            const payload = [enemy.id, position.x, position.y, 0];
            this.players.forEach((player) => player.connection.queueMessage(new Message(utils.getTypeByName(protocol, "GAME_HIT").id, payload)));
            this.swapActivePlayer();
        });
    }

    /**
     * registers a new {@link Player}
     * @param {Connection} connection - Websocket {@link Connection} for the player
     * @returns Player
     */
    addPlayer(connection) {
        // Add player
        let newPlayer = new Player(this.players.length, connection);
        this.players.push(newPlayer);

        if (this.players.length >= 3) {
            console.log((new Date()) + ' Connection refused.');
            newPlayer.connection.sendMessage(new Message(utils.getTypeByName(protocol, "SESSION_REFUSED").id));
        }
        else if (this.players.length == 2) {
            // start session
            this.players.forEach((p) => p.connection.queueMessage(new Message(utils.getTypeByName(protocol, "SESSION_STARTED").id)));
            //game init
            for (let player of this.players) {
                player.connection.queueMessage(new Message(utils.getTypeByName(protocol, "GAME_INIT").id, [player.id]));
            }

            this.init();
        }
        else if (this.players.length == 1) {
            // session waiting
            this.players[0].connection.queueMessage(new Message(utils.getTypeByName(protocol, "SESSION_WAITING").id));
        }

        return newPlayer;
    }

    /**
     * swaps the active player
     */
    swapActivePlayer() {
        this.activePlayer = this.activePlayer.equals(this.players[0]) ? this.players[1] : this.players[0];
        this.gameTurn();
    }

    /**
     * next game turn. If the game over state is reached no more game_turn events are emitted
     */
    gameTurn() {
        if (this.isGameOver()) {
            return this.gameOver(); // game is over -> no more turns
        }
        this.players.forEach((player) => player.connection.queueMessage(new Message(utils.getTypeByName(protocol, "GAME_TURN").id, [this.activePlayer.id])));
    }

    /**
     * sets the battleships for a player randomly
     * @param { Player } player - the player to set the ships for
     */
    setBattleShips(player) {
        /**
        * valid positions to place a ship
        * @type { bool[][] }
        */
        const allowedPositions = Array.from(Array(this.fieldSize), () => new Array(this.fieldSize).fill(true));

        for (const ship of this.shipSizes) {
            for (let i = 0; i < ship.amount; i++) {
                const battleShip = this.createBattleShip(allowedPositions, ship.length);

                player.defenseField.push(battleShip);
            }
        }
    }

    /**
     * creates a new battleship. Throws an expcetion if max iterations is reached and no ship could be placed
     * @param {  bool[][] } allowedPositions - positions where no ship is placed yet
     * @param { int } shipLength - the length of the ship to create
     * @returns BattleShip
     */
    createBattleShip(allowedPositions, shipLength) {
        let isHorizontal = Math.round(Math.random()) === 1 ? true:false;
        let counter = 0;

        do {
            if (isHorizontal) {
                var position = Position.random(this.fieldSize - shipLength, this.fieldSize);
            }
            else {
                var position = Position.random(this.fieldSize, this.fieldSize - shipLength);
            }

            var battleShip = new Battleship(position, shipLength, isHorizontal);

            if (counter > this.createBattleShipMaxIterations) {
                throw 'MAX ITERATIONS REACHED! (too many ships for too few cells)';
            }

            counter++;
        }
        while (!this.isBattleShipValid(allowedPositions, battleShip));


        this.updateAllowedPositions(allowedPositions, battleShip);

        return battleShip;
    }

    /**
     * checks whether the occupied cells of the battleship overlap with others
     * @param { bool[][] } allowedPositions - positions where no ship is placed yet
     * @param { BattleShip } battleShip - the battleship to check
     * @returns bool
     */
    isBattleShipValid(allowedPositions, battleShip) {
        for (const shipCell of battleShip.shipCells) {
            const pos = shipCell.position;
            if (!allowedPositions[pos.x][pos.y]) {
                return false;
            }
        }

        return true;
    }

    /**
     * sets the surrounding cells of the battleship to invalid
     * @param { bool[][] } allowedPositions - positions where no ship is placed yet
     * @param { BattleShip } battleShip
     */
    updateAllowedPositions(allowedPositions, battleShip) {
        battleShip.shipCells.forEach(shipCell => {
            for (let dx = -1; dx <= 1; dx++) {
                for (let dy = -1; dy <= 1; dy++) {
                    const pos = shipCell.position;
                    const x = pos.x + dx;
                    const y = pos.y + dy;
                    if (x < 0 || x >= this.fieldSize || y < 0 || y >= this.fieldSize) {
                        continue;
                    }
                    allowedPositions[x][y] = false;
                }
            }
        });
    }

    /**
     * converts the defensefield of the player into a 1D array (0 = water, 1 = ship)
     * @param { Player } player
     * @returns int[]
     */
    serializeDefenseField(player) {
        var array = Array.from(Array(this.fieldSize), () => new Array(this.fieldSize).fill(0));
        
        for (const ship of player.defenseField) {
            for (const cell of ship.shipCells) {                
                array[cell.position.x][cell.position.y] = 1;
            }
        }

        return array.flat();    
    }

    /**
     * returns whether a player won
     */
    isGameOver() {
        return this.players[1 - this.activePlayer.id].isDefeated();
    }

    /**
     * emits the game_over event
     */
    gameOver() {
        console.log(`player ${this.activePlayer.id} won!`);
        this.players.forEach((player) => player.connection.queueMessage(new Message(utils.getTypeByName(protocol, "GAME_OVER").id, [this.activePlayer.id])));
    }
}

module.exports.GameControllerServer = GameControllerServer;
