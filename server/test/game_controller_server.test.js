const expect = require('chai').expect;

const GameControllerServer = require('../game_controller_server.js').GameControllerServer;
const Player = require('../../modules/player');

const shipSizes = [
    { length: 2, amount: 3 },
    { length: 3, amount: 2 },
    { length: 4, amount: 1 },
];

const fieldSize = 10;

const gameController = new GameControllerServer(fieldSize, shipSizes);
const fakeConnection = { on() {} };

describe('game_controller_sever.js', function () {
    // init game
    gameController.players = [];
    gameController.players.push(new Player(0, fakeConnection));
    gameController.players.push(new Player(1, fakeConnection));
    gameController.init();

    describe('#init()', function () {
        it('A player should have the expected amount of ships', function () {
            // count the amount of ships, that should be generated
            let shipCount = 0;
            gameController.shipSizes.forEach(ship => shipCount += ship.amount );
            
            expect(shipCount === gameController.players[0].defenseField.length).to.be.true;
        });
        it ('Every battleship should have as many battlefieldcells as it is long', function () {
            for (const player of gameController.players) {
                for (const ship of player.defenseField) {
                    expect(ship.length === ship.shipCells.length).to.be.true;
                }    
            }
        });
    });
    describe ('#serializeDefenseField(player)', function () {
        it ('Should return an array of size fieldSize*fieldSize', function () {
            for (const player of gameController.players) {                
                const field = gameController.serializeDefenseField(player);
                expect(field.length === fieldSize**2).to.be.true;
            }
        });        
    });
});