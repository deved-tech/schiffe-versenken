# Battleships

Documentation:
https://deved-tech.gitlab.io/schiffe-versenken

# Installation

### Client:

1. Copy ./modules and ./client directory

1. Run the following commands

    `cd modules` & 
    `npm install`

    `cd ../client` & 
    `npm install`

### Server:

If you also want to host the server yourself follow these steps:

1. Copy ./server directory

1. Run the following commands:

    `cd server` & 
    `npm install`

1. To run the server execute
    
    `cd server` & 
    `node server.js`


# How to play

1. To run the client execute
    
    `cd client` & 
    `node client.js`

1. Enter the WebSocket Address to the desired server ws://battleships.programonaut.com if necessary, otherwise press enter.

1. Once all players are connected, the active player should look like this: 

    <img src="documentation/game_start.png" alt="drawing" width="1200"/>

1. Enter row and column to attack (or wait until it is your turn)

1. Once all ships are destroyed the game is over and you have to reconnect to start a new game

1. Console interface legend: 

    <img src="documentation/game_legend.png" alt="drawing" width="500"/>


You may not want to scroll with in the console, as the game visualization may corrupt (it will fix itself in the next turn)


## Game Procedure Server and Client

![](documentation/process_diagram_server.png)


## Class Diagram (UML Notation)

![](documentation/UML.png)
